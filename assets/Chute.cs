﻿using UnityEngine;
using System.Collections;

public class Chute : MonoBehaviour {

	public float KickPower;
	public string KickButton;
	public float KickHeightPower;
	public string KickHeightAxis;
	public double range;
	public int maxPower;
	int i=0;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
				float Y = Input.GetAxis (KickHeightAxis);
				GameObject bola = GameObject.Find ("Bola");
				GameObject gui = GameObject.Find ("PowerValue");
				float distance = Vector3.Distance (gameObject.transform.position, bola.transform.position);
				if (Input.GetKey (KickButton)) {
					if (i<maxPower)	
						i++;
				} else {
				if (distance < range) {
					Vector3 vec = gameObject.transform.forward;
					bola.rigidbody.AddForce (i * KickPower * vec.x, i * Y * KickHeightPower, i * KickPower * vec.z);
						}	
				i = 0;
				}
		gui.guiText.text = ""+i;
		}
}