﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	public float translationCoef;
	public float rotationCoef;
	public float maxSpeed;
	public string vAxis;
	public string hAxis;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float x = Input.GetAxis (hAxis);
		float z = Input.GetAxis (vAxis);
		Vector3 frente = gameObject.transform.forward;
		Vector3 speed = gameObject.rigidbody.velocity;
		if (speed.magnitude < maxSpeed) {
			gameObject.rigidbody.AddForce (z * translationCoef * frente);
		}
		gameObject.transform.Rotate (0, rotationCoef * x, 0);
	}
}